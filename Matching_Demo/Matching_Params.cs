﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BrightIdeasSoftware;

namespace Matching_Demo
{
    public class Params
    {
        public string sName { get; set; }
        public string sValue { get; set; }

        public Params(string name, string value)
        {
            sName = name;
            sValue = value;
        }
    }



    public class Matching_Params
    {
        public string[] sProperties, sValues;
        int nParams = 8;

        public Matching_Params()
        {
            sProperties = new string[nParams];
            sProperties[0] = "Angle Start";
            sProperties[1] = "Angle Stop";
            sProperties[2] = "Angle Step";
            sProperties[3] = "Scale Start";
            sProperties[4] = "Scale Stop";
            sProperties[5] = "Scale Step";
            sProperties[6] = "Min Score";
            sProperties[7] = "Dark On Light";

            sValues = new string[nParams];
            sValues[0] = "-10";
            sValues[1] = "10";
            sValues[2] = "2";
            sValues[3] = "0.98";
            sValues[4] = "1.02";
            sValues[5] = "0.01";
            sValues[6] = "70";
            sValues[7] = "true";
        }

        public void InitializeParameters(ObjectListView olv)
        {
            //Add items in the listview
            List<Params> lParams = new List<Params>();
            for (int i = 0; i < nParams; i++)
                lParams.Add(new Params(sProperties[i], sValues[i]));

            olv.SetObjects(lParams);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HalconDotNet;

namespace Matching_Demo
{
    public class Halcon_Utilities
    {
        public static HImage SetImagePtr(int width, int height, IntPtr imagePtr)
        {
            HImage source;
            try
            {
                HImage temp = new HImage("byte", Matching_Utilities.Stride(width), height, imagePtr);
                source = temp.CropPart(0, 0, width, height);
                temp.Dispose();
                //temp.to
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return source;
        }
    }
}

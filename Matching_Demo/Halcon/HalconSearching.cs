﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using HalconDotNet;

namespace Matching_Demo
{
    public class HalconSearching
    {
        public string m_id;
        public HTuple hv_ModelID;

        //private parameters
        private Size ModelSize;

        //public parameters
        public float AcceptScore;   //minimum score 

        public float MaxRotation = 0, MinRotation = 0;
        public float RotationIncr = 1;  //Rotation increment

        public float MaxScale = 1, MinScale = 1;
        public float ScaleIncr = 0.1F;  //Scale increment

        //public results
        public float Score = 0;  //result's score
        public float Scale = 1;  //result's Scale
        public float Angle = 0;    //result's angle
        public PointF MatchPosition;   //Center
        public PointF[] RecResult = new PointF[4];  //Coordinate of results
        public bool LearningDone;

        public HalconSearching(string id, int nThread)
        {
            m_id = id;
        }

        public void AddModel(byte[] model, Size szModel, float minRot, float maxRot, float rotIncr,
           float minScale, float maxScale, float scaleIncr, float acceptScore = 50, int maxCan = 1)
        {
            hv_ModelID = new HTuple();

            IntPtr ptr;
            Matching_Utilities.byteToIntptr(model, out ptr);
            HImage ho_ImageReduced = Halcon_Utilities.SetImagePtr(szModel.Width, szModel.Height, ptr);
            float fScale = Matching_Utilities.RAD_PER_DEG;

            ModelSize = szModel;
            MaxRotation = maxRot;
            MinRotation = minRot;
            RotationIncr = rotIncr;
            MaxScale = maxScale;
            MinScale = minScale;
            ScaleIncr = scaleIncr;
            AcceptScore = acceptScore;

            HOperatorSet.CreateNccModel(ho_ImageReduced, 5, minRot * fScale, maxRot * fScale, rotIncr * fScale, "ignore_global_polarity", out hv_ModelID);
            LearningDone = true;

            // HOperatorSet.WriteNccModel(hv_ModelID, sFiduPath);
        }

        public void Execute(byte[] src, Size szSrc)
        {
            IntPtr ptr;
            Matching_Utilities.byteToIntptr(src, out ptr);
            HImage ho_ImageSearch = Halcon_Utilities.SetImagePtr(szSrc.Width, szSrc.Height, ptr);

            //Bitmap origin = new Bitmap(sFile);

            HTuple hv_Row = new HTuple(), hv_Column = new HTuple();
            HTuple hv_Angle = new HTuple(), hv_Scale = new HTuple();
            HTuple hv_Score = new HTuple();

            //HOperatorSet.ReadNccModel(sFiduPath, out hv_ModelID);

            HOperatorSet.FindScaledShapeModel(ho_ImageSearch, hv_ModelID, (new HTuple(-45)).TupleRad()
                , (new HTuple(90)).TupleRad(), 0.8, 1.0, 0.4, 0, 0.5, "least_squares",
                5, 0.8, out hv_Row, out hv_Column, out hv_Angle, out hv_Scale, out hv_Score);

            //Bitmap binaryImage = ResizeBitmap(origin, origin.Width, origin.Height);
            //using (Graphics g = Graphics.FromImage(binaryImage))
            //{
            //    for (int i = 0; i < hv_Angle.Length; i++)
            //    {
            //        double angle = hv_Angle.DArr[i] / 0.0174532F;
            //        if (angle < 1)
            //        {
            //            int x = (int)(hv_Column.DArr[i] - 6);
            //            int y = (int)(hv_Row.DArr[i] - 6);
            //            int width = (int)(12);
            //            int height = (int)(12);
            //            Rectangle roi = new Rectangle(x, y, width, height);
            //            g.DrawEllipse(new Pen(Color.Red), roi);
            //        }
            //    }
            //}

            HObject ho_ModelContours = null;
            HOperatorSet.GenEmptyObj(out ho_ModelContours);
            ho_ModelContours.Dispose();
            HOperatorSet.GetShapeModelContours(out ho_ModelContours, hv_ModelID, 1);
            //pbMain.Image = binaryImage;
        }

        }
}

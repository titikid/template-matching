﻿namespace Matching_Demo
{
    partial class Matching_Demo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonLoadSource = new System.Windows.Forms.Button();
            this.buttonTeaching = new System.Windows.Forms.Button();
            this.buttonMatching = new System.Windows.Forms.Button();
            this.buttonLoadTemplate = new System.Windows.Forms.Button();
            this.buttonTest = new System.Windows.Forms.Button();
            this.pbSource = new System.Windows.Forms.PictureBox();
            this.pbTemplate = new System.Windows.Forms.PictureBox();
            this.labelScore = new System.Windows.Forms.Label();
            this.comboBoxLib = new System.Windows.Forms.ComboBox();
            this.buttonUp = new System.Windows.Forms.Button();
            this.buttonDown = new System.Windows.Forms.Button();
            this.objectListViewParams = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.pbSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectListViewParams)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonLoadSource
            // 
            this.buttonLoadSource.BackColor = System.Drawing.Color.White;
            this.buttonLoadSource.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoadSource.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoadSource.Location = new System.Drawing.Point(11, 15);
            this.buttonLoadSource.Margin = new System.Windows.Forms.Padding(2);
            this.buttonLoadSource.Name = "buttonLoadSource";
            this.buttonLoadSource.Size = new System.Drawing.Size(90, 44);
            this.buttonLoadSource.TabIndex = 4;
            this.buttonLoadSource.Text = "Load Source";
            this.buttonLoadSource.UseVisualStyleBackColor = false;
            this.buttonLoadSource.Click += new System.EventHandler(this.buttonLoadSource_Click);
            // 
            // buttonTeaching
            // 
            this.buttonTeaching.BackColor = System.Drawing.Color.Gray;
            this.buttonTeaching.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTeaching.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTeaching.Location = new System.Drawing.Point(12, 77);
            this.buttonTeaching.Margin = new System.Windows.Forms.Padding(2);
            this.buttonTeaching.Name = "buttonTeaching";
            this.buttonTeaching.Size = new System.Drawing.Size(90, 42);
            this.buttonTeaching.TabIndex = 24;
            this.buttonTeaching.Text = "Teaching";
            this.buttonTeaching.UseVisualStyleBackColor = false;
            this.buttonTeaching.Click += new System.EventHandler(this.buttonTeaching_Click);
            // 
            // buttonMatching
            // 
            this.buttonMatching.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.buttonMatching.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMatching.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMatching.Location = new System.Drawing.Point(120, 77);
            this.buttonMatching.Margin = new System.Windows.Forms.Padding(2);
            this.buttonMatching.Name = "buttonMatching";
            this.buttonMatching.Size = new System.Drawing.Size(90, 42);
            this.buttonMatching.TabIndex = 23;
            this.buttonMatching.Text = "Searching";
            this.buttonMatching.UseVisualStyleBackColor = false;
            this.buttonMatching.Click += new System.EventHandler(this.buttonMatching_Click);
            // 
            // buttonLoadTemplate
            // 
            this.buttonLoadTemplate.BackColor = System.Drawing.Color.White;
            this.buttonLoadTemplate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoadTemplate.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoadTemplate.Location = new System.Drawing.Point(120, 15);
            this.buttonLoadTemplate.Margin = new System.Windows.Forms.Padding(2);
            this.buttonLoadTemplate.Name = "buttonLoadTemplate";
            this.buttonLoadTemplate.Size = new System.Drawing.Size(90, 44);
            this.buttonLoadTemplate.TabIndex = 22;
            this.buttonLoadTemplate.Text = "Load Template";
            this.buttonLoadTemplate.UseVisualStyleBackColor = false;
            this.buttonLoadTemplate.Click += new System.EventHandler(this.buttonLoadTemplate_Click);
            // 
            // buttonTest
            // 
            this.buttonTest.BackColor = System.Drawing.Color.White;
            this.buttonTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonTest.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTest.Location = new System.Drawing.Point(120, 136);
            this.buttonTest.Margin = new System.Windows.Forms.Padding(2);
            this.buttonTest.Name = "buttonTest";
            this.buttonTest.Size = new System.Drawing.Size(90, 42);
            this.buttonTest.TabIndex = 21;
            this.buttonTest.Text = "Test";
            this.buttonTest.UseVisualStyleBackColor = false;
            // 
            // pbSource
            // 
            this.pbSource.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbSource.Location = new System.Drawing.Point(231, 12);
            this.pbSource.Margin = new System.Windows.Forms.Padding(2);
            this.pbSource.Name = "pbSource";
            this.pbSource.Size = new System.Drawing.Size(600, 500);
            this.pbSource.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbSource.TabIndex = 26;
            this.pbSource.TabStop = false;
            // 
            // pbTemplate
            // 
            this.pbTemplate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbTemplate.Location = new System.Drawing.Point(853, 61);
            this.pbTemplate.Margin = new System.Windows.Forms.Padding(2);
            this.pbTemplate.Name = "pbTemplate";
            this.pbTemplate.Size = new System.Drawing.Size(250, 180);
            this.pbTemplate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbTemplate.TabIndex = 27;
            this.pbTemplate.TabStop = false;
            // 
            // labelScore
            // 
            this.labelScore.AutoSize = true;
            this.labelScore.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScore.Location = new System.Drawing.Point(852, 324);
            this.labelScore.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelScore.Name = "labelScore";
            this.labelScore.Size = new System.Drawing.Size(48, 19);
            this.labelScore.TabIndex = 28;
            this.labelScore.Text = "Score:";
            // 
            // comboBoxLib
            // 
            this.comboBoxLib.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxLib.FormattingEnabled = true;
            this.comboBoxLib.Items.AddRange(new object[] {
            "Halcon",
            "IPP"});
            this.comboBoxLib.Location = new System.Drawing.Point(89, 199);
            this.comboBoxLib.Name = "comboBoxLib";
            this.comboBoxLib.Size = new System.Drawing.Size(121, 23);
            this.comboBoxLib.TabIndex = 29;
            this.comboBoxLib.SelectedIndexChanged += new System.EventHandler(this.comboBoxLib_SelectedIndexChanged);
            // 
            // buttonUp
            // 
            this.buttonUp.BackColor = System.Drawing.Color.White;
            this.buttonUp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUp.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUp.Location = new System.Drawing.Point(950, 12);
            this.buttonUp.Margin = new System.Windows.Forms.Padding(2);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(46, 33);
            this.buttonUp.TabIndex = 30;
            this.buttonUp.Text = "Up";
            this.buttonUp.UseVisualStyleBackColor = false;
            this.buttonUp.Click += new System.EventHandler(this.buttonUp_Click);
            // 
            // buttonDown
            // 
            this.buttonDown.BackColor = System.Drawing.Color.White;
            this.buttonDown.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDown.Font = new System.Drawing.Font("Calibri", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDown.Location = new System.Drawing.Point(947, 254);
            this.buttonDown.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(58, 32);
            this.buttonDown.TabIndex = 31;
            this.buttonDown.Text = "Down";
            this.buttonDown.UseVisualStyleBackColor = false;
            this.buttonDown.Click += new System.EventHandler(this.buttonDown_Click);
            // 
            // objectListViewParams
            // 
            this.objectListViewParams.AllColumns.Add(this.olvColumn1);
            this.objectListViewParams.AllColumns.Add(this.olvColumn2);
            this.objectListViewParams.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.objectListViewParams.CellEditActivation = BrightIdeasSoftware.ObjectListView.CellEditActivateMode.DoubleClick;
            this.objectListViewParams.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn1,
            this.olvColumn2});
            this.objectListViewParams.Cursor = System.Windows.Forms.Cursors.Default;
            this.objectListViewParams.GridLines = true;
            this.objectListViewParams.Location = new System.Drawing.Point(11, 244);
            this.objectListViewParams.Name = "objectListViewParams";
            this.objectListViewParams.Size = new System.Drawing.Size(199, 268);
            this.objectListViewParams.TabIndex = 32;
            this.objectListViewParams.UseCompatibleStateImageBehavior = false;
            this.objectListViewParams.View = System.Windows.Forms.View.Details;
            this.objectListViewParams.CellEditFinished += new BrightIdeasSoftware.CellEditEventHandler(this.objectListViewParams_CellEditFinished);
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "sName";
            this.olvColumn1.Groupable = false;
            this.olvColumn1.Text = "Properties";
            this.olvColumn1.Width = 120;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "sValue";
            this.olvColumn2.Text = "Value";
            // 
            // Matching_Demo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(1123, 525);
            this.Controls.Add(this.objectListViewParams);
            this.Controls.Add(this.buttonDown);
            this.Controls.Add(this.buttonUp);
            this.Controls.Add(this.comboBoxLib);
            this.Controls.Add(this.labelScore);
            this.Controls.Add(this.pbTemplate);
            this.Controls.Add(this.pbSource);
            this.Controls.Add(this.buttonTeaching);
            this.Controls.Add(this.buttonMatching);
            this.Controls.Add(this.buttonLoadTemplate);
            this.Controls.Add(this.buttonTest);
            this.Controls.Add(this.buttonLoadSource);
            this.Name = "Matching_Demo";
            this.Text = "Matching Demo";
            ((System.ComponentModel.ISupportInitialize)(this.pbSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.objectListViewParams)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonLoadSource;
        private System.Windows.Forms.Button buttonTeaching;
        private System.Windows.Forms.Button buttonMatching;
        private System.Windows.Forms.Button buttonLoadTemplate;
        private System.Windows.Forms.Button buttonTest;
        private System.Windows.Forms.PictureBox pbSource;
        private System.Windows.Forms.PictureBox pbTemplate;
        private System.Windows.Forms.Label labelScore;
        private System.Windows.Forms.ComboBox comboBoxLib;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Button buttonDown;
        private BrightIdeasSoftware.ObjectListView objectListViewParams;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PropertyGridEx;

namespace Matching_Demo
{
    public enum eGrid { eInit, eUpdate, eRead, eWrite, eOpen, eSave }

    public class ezGrid
    {
        bool m_bForeign = false;
        string m_id, m_model, m_strGroup, m_strProp, m_strSub;
        object m_objOld, m_objNew;
        //ezRegistry m_reg = null;
        eGrid m_mode;
        PropertyGridEx.PropertyGridEx m_grid = null;

        public ezGrid(string id, PropertyGridEx.PropertyGridEx grid, bool bForeign)
        {
            m_id = id; m_grid = grid; m_bForeign = bForeign;
        }

        public void Update(eGrid mode)
        {
            m_mode = mode;
            m_grid.ShowCustomProperties = true;
            if (IsInit()) m_grid.Item.Clear();
        }

        public bool IsInit()
        {
            return (m_mode == eGrid.eInit) || (m_mode == eGrid.eRead) || (m_mode == eGrid.eOpen);
        }

        public void PropertyChange(PropertyValueChangedEventArgs e)
        {
            GridItem gridItem;
            if (e.ChangedItem.Parent.Parent.Parent == null)
            {
                m_strSub = "";
                gridItem = e.ChangedItem;
            }
            else
            {
                m_strSub = e.ChangedItem.Label;
                gridItem = e.ChangedItem.Parent;
            }
            m_strGroup = gridItem.Parent.Label;
            m_strProp = gridItem.Label;
            m_objNew = gridItem.Value;
            m_objOld = e.OldValue;
        }

        public bool Set(bool b, string strGroup, string strProp, string strDesc, bool bReadOnly = false)
        {
            switch (m_mode)
            {
                //   case eGrid.eRead: b = m_reg.Read(strGroup + strProp, b); break;
                //  case eGrid.eWrite: m_reg.Write(strGroup + strProp, b); break;
                case eGrid.eInit: break;
                case eGrid.eUpdate:
                    strGroup = GetID(strGroup); strProp = GetID(strProp);
                    if ((strGroup == m_strGroup) && (strProp == m_strProp))
                    {
                        // m_log.Add(strGroup + strProp + m_strSub + " : " + b + " -> ", m_objNew.ToString());
                        return (bool)m_objNew;
                    }
                    break;
            }
            if (IsInit())
            {
                strGroup = GetID(strGroup); strProp = GetID(strProp);
                m_grid.Item.Add(strProp, b, bReadOnly, strGroup, strDesc, true);
            }
            return b;
        }

        public long Set(long l, string strGroup, string strProp, string strDesc, bool bReadOnly = false)
        {
            switch (m_mode)
            {
                // case eGrid.eWrite: m_reg.Write(strGroup + strProp, l); break;
                case eGrid.eInit: break;
                case eGrid.eUpdate:
                    strGroup = GetID(strGroup); strProp = GetID(strProp);
                    if ((strGroup == m_strGroup) && (strProp == m_strProp))
                    {
                        // m_log.Add(strGroup + strProp + m_strSub + " : " + l + " -> ", m_objNew.ToString());
                        return (long)m_objNew;
                    }
                    break;
            }
            if (IsInit())
            {
                strGroup = GetID(strGroup); strProp = GetID(strProp);
                m_grid.Item.Add(strProp, l, bReadOnly, strGroup, strDesc, true);
            }
            return l;
        }

        public double Set(double f, string strGroup, string strProp, string strDesc, bool bReadOnly = false)
        {
            switch (m_mode)
            {
                //     case eGrid.eRead: f = m_reg.Read(strGroup + strProp, f); break;
                //    case eGrid.eWrite: m_reg.Write(strGroup + strProp, f); break;
                case eGrid.eInit: break;
                case eGrid.eUpdate:
                    strGroup = GetID(strGroup); strProp = GetID(strProp);
                    if ((strGroup == m_strGroup) && (strProp == m_strProp))
                    {
                        //   m_log.Add(strGroup + strProp + m_strSub + " : " + f + " -> ", m_objNew.ToString());
                        return (double)m_objNew;
                    }
                    break;
            }
            if (IsInit())
            {
                strGroup = GetID(strGroup); strProp = GetID(strProp);
                m_grid.Item.Add(strProp, f, bReadOnly, strGroup, strDesc, true);
            }
            return f;
        }

        public string Set(string str, string strGroup, string strProp, string strDesc, bool bReadOnly = false)
        {
            switch (m_mode)
            {
                //case eGrid.eRead: str = m_reg.Read(strGroup + strProp, str); break;
                //case eGrid.eWrite: m_reg.Write(strGroup + strProp, str); break;
                case eGrid.eInit: break;
                case eGrid.eUpdate:
                    strGroup = GetID(strGroup); strProp = GetID(strProp);
                    if ((strGroup == m_strGroup) && (strProp == m_strProp))
                    {
                        // m_log.Add(strGroup + strProp + m_strSub + " : " + str + " -> ", m_objNew.ToString());
                        return (string)m_objNew;
                    }
                    break;
            }
            if (IsInit())
            {
                strGroup = GetID(strGroup); strProp = GetID(strProp);
                m_grid.Item.Add(strProp, str, bReadOnly, strGroup, strDesc, true);
            }
            return str;
        }

        public void Refresh()
        {
            if (IsInit()) m_grid.Refresh();
        }

        string GetID(string str)
        {
            string[] strLines;
            if (!m_bForeign) return str;
            strLines = str.Split(new char[] { '.' });
            switch (strLines.Length)
            {
                /*
                case 1: if (IsInit()) 
                    str = m_reg.Read("ID_" + str, str);  return str;
                case 2: if (IsInit()) 
                    strLines[1] = m_reg.Read("ID_" + strLines[1], strLines[1]);
                    return strLines[0] + "." + strLines[1];
                 */
                default: return str;
            }
        }

    }
}

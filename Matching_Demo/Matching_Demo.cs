﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using BrightIdeasSoftware;
//using HalconDotNet;

namespace Matching_Demo
{
    public partial class Matching_Demo : Form
    {
        Matching_Params parameters = new Matching_Params();
        string[] templateFiles;
        Bitmap source;
        byte[] sourceData;
        IPPSearching iSearch = new IPPSearching(1);
        HalconSearching hSearch = new HalconSearching("halconSearch", 1);
        Size mainSize, templateSize;
        Point mainLoc, templateLoc;
        int nTempIdx, nTotalTemp;
        long executionTime;

        public Matching_Demo()
        {
            InitializeComponent();
            parameters.InitializeParameters(objectListViewParams);

            comboBoxLib.SelectedIndex = 1;
            mainSize = new Size(600, 500);
            templateSize = new Size(250, 180);
            mainLoc = pbSource.Location;
            templateLoc = pbTemplate.Location;
            nTempIdx = -1;
            nTotalTemp = -1; //no file
        }



        private void buttonLoadSource_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "*.jpg ,*.png ,*.bmp,*.gif | *.jpg;*.png ;*.bmp;*.gif;";
            if (openFile.ShowDialog() != DialogResult.OK) return;

            source = new Bitmap(openFile.FileName);
            sourceData = Matching_Utilities.BitmapToByteArray(source);
            RedrawUI(pbSource, mainSize, mainLoc, openFile.FileName);
        }

        private void buttonLoadTemplate_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Filter = "*.jpg ,*.png ,*.bmp,*.gif | *.jpg;*.png ;*.bmp;*.gif;";
            openFile.Multiselect = true;
            if (openFile.ShowDialog() != DialogResult.OK) return;

            templateFiles = openFile.FileNames;
            nTotalTemp = templateFiles.GetLength(0);
            RedrawUI(pbTemplate, templateSize, templateLoc, openFile.FileName);
            pbSource.Image = source;
            buttonTeaching.BackColor = Color.Gray;

        }

        private void RedrawUI(PictureBox pb, Size baseSize, Point baseLoc, string filename)
        {
            Bitmap temp = new Bitmap(filename);
            byte[] bTemp = Matching_Utilities.BitmapToByteArray(temp);

            float ratio = (float)temp.Height / (float)temp.Width;
            float baseRatio = (float)baseSize.Height / (float)baseSize.Width;
            if (ratio > baseRatio)
            {
                int newWidth = (int)(baseSize.Height / ratio);
                pb.Location = new Point(baseLoc.X + (baseSize.Width - newWidth) / 2, baseLoc.Y);
                pb.Size = new Size(newWidth, baseSize.Height);
            }
            else
            {
                int newHeight = (int)(baseSize.Width * ratio);
                pb.Location = new Point(baseLoc.X, baseLoc.Y + (baseSize.Height - newHeight) / 2);
                pb.Size = new Size(baseSize.Width, newHeight);
            }

            pb.Image = temp;
        }

        private void buttonTeaching_Click(object sender, EventArgs e)
        {
            iSearch.ClearModel();
            for (int n = 0; n < nTotalTemp; n++)
            {
                Bitmap temp = new Bitmap(templateFiles[n]);
                Byte[] bTemp = Matching_Utilities.BitmapToByteArray(temp);

                iSearch.AddModel(0, bTemp, temp.Size,
                    float.Parse(parameters.sValues[0]), //"Angle Start"
                    float.Parse(parameters.sValues[1]), //"Angle Stop"
                    float.Parse(parameters.sValues[2]), //"Angle Step"
                    float.Parse(parameters.sValues[3]), //"Scale Start"
                    float.Parse(parameters.sValues[4]), //"Scale Stop"
                    float.Parse(parameters.sValues[5]), //"Scale Step"
                    float.Parse(parameters.sValues[6]));//"Min Score"

                nTempIdx = 0;
            }

            //iSearch.ModelList[5].m_ActivatedRotation[8] = false;

            buttonTeaching.BackColor = Color.Yellow;
        }

        private void buttonMatching_Click(object sender, EventArgs e)
        {
            if (nTempIdx > -1)
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                iSearch.Execute(sourceData, source.Size);
                sw.Stop();
                executionTime = sw.ElapsedMilliseconds;
                Console.WriteLine("Time(ms): " + executionTime.ToString());
                fnDrawResult();
            }
        }

        private void comboBoxLib_SelectedIndexChanged(object sender, EventArgs e)
        {
            //RedesignUI(comboBoxLib.SelectedIndex);
        }


        private void fnDrawResult()
        {
            pbSource.Refresh();
            if (iSearch.BestMatchIdx < 0)
            {
                labelScore.Text = "Not found";
                return;
            }
            float xScale = (float)(pbSource.Width) / (float)(source.Width);
            float yScale = (float)(pbSource.Height) / (float)(source.Height);

            Point leftTop = new Point((int)(xScale * iSearch.ModelList[iSearch.BestMatchIdx].RecResult[0].X), (int)(yScale * iSearch.ModelList[iSearch.BestMatchIdx].RecResult[0].Y));
            Point rightTop = new Point((int)(xScale * iSearch.ModelList[iSearch.BestMatchIdx].RecResult[1].X), (int)(yScale * iSearch.ModelList[iSearch.BestMatchIdx].RecResult[1].Y));
            Point rightBot = new Point((int)(xScale * iSearch.ModelList[iSearch.BestMatchIdx].RecResult[2].X), (int)(yScale * iSearch.ModelList[iSearch.BestMatchIdx].RecResult[2].Y));
            Point leftBot = new Point((int)(xScale * iSearch.ModelList[iSearch.BestMatchIdx].RecResult[3].X), (int)(yScale * iSearch.ModelList[iSearch.BestMatchIdx].RecResult[3].Y));

            using (Graphics gr = pbSource.CreateGraphics())
            {
                gr.DrawLine(new Pen(Color.Red), leftTop, rightTop);
                gr.DrawLine(new Pen(Color.Red), rightTop, rightBot);
                gr.DrawLine(new Pen(Color.Red), rightBot, leftBot);
                gr.DrawLine(new Pen(Color.Red), leftBot, leftTop);
            }

            nTempIdx = iSearch.BestMatchIdx;
            RedrawUI(pbTemplate, templateSize, templateLoc, templateFiles[iSearch.BestMatchIdx]);

            labelScore.Text = "Position: " + iSearch.ModelList[iSearch.BestMatchIdx].MatchPosition.ToString() + "\nAngle: " + iSearch.ModelList[iSearch.BestMatchIdx].Angle.ToString() +
             "\nScale: " + iSearch.ModelList[iSearch.BestMatchIdx].Scale.ToString() + "\nScore: " + iSearch.ModelList[iSearch.BestMatchIdx].Score.ToString() + "\nLeft Top: " +
             iSearch.ModelList[iSearch.BestMatchIdx].RecResult[0].X.ToString() + ", " + iSearch.ModelList[iSearch.BestMatchIdx].RecResult[0].Y.ToString() + "\nTime (ms): " + executionTime.ToString();

        }

        private void DrawBitmap(ref Bitmap src)
        {
            //Image src_new = new Bitmap(src);
            //using (Graphics grp = Graphics.FromImage(src_new))
            //{
            //    //grp.DrawEllipse(new Pen(Color.Red), new Rectangle(0, 0, 20, 20));
            //}
            //src = new Bitmap(src_new);
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            if (nTotalTemp > -1)
            {
                nTempIdx++;
                if (nTempIdx > nTotalTemp - 1)
                    nTempIdx = 0;
                RedrawUI(pbTemplate, templateSize, templateLoc, templateFiles[nTempIdx]);
            }
        }

        private void buttonDown_Click(object sender, EventArgs e)
        {
            if (nTotalTemp > -1)
            {
                nTempIdx--;
                if (nTempIdx < 0)
                    nTempIdx = nTotalTemp - 1;
                RedrawUI(pbTemplate, templateSize, templateLoc, templateFiles[nTempIdx]);
            }
        }

        private void objectListViewParams_CellEditFinished(object sender, CellEditEventArgs e)
        {
            buttonTeaching.BackColor = Color.Gray;
            parameters.sValues[e.ListViewItem.Index] = e.NewValue.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Matching_Demo
{
    public class Matching_Utilities
    {
        public const float RAD_PER_DEG = 0.0174532F; 
        public static byte[] BitmapToByteArray(Bitmap bitmap)
        {

            BitmapData bmpdata = null;

            try
            {
                bmpdata = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.ReadOnly, bitmap.PixelFormat);
                int numbytes = bmpdata.Stride * bitmap.Height;
                byte[] bytedata = new byte[numbytes];
                IntPtr ptr = bmpdata.Scan0;

                Marshal.Copy(ptr, bytedata, 0, numbytes);

                return bytedata;
            }
            finally
            {
                if (bmpdata != null)
                    bitmap.UnlockBits(bmpdata);
            }

        }

        public static int Stride(int nWidth)
        {
            return 4 * (1 + (nWidth - 1) / 4);
        }

        public static void byteToIntptr(byte[] input, out IntPtr pointer)
        {
            GCHandle pinnedArray = GCHandle.Alloc(input, GCHandleType.Pinned);
            pointer = pinnedArray.AddrOfPinnedObject();
            // Do your stuff...
            pinnedArray.Free();
        }
    }
}

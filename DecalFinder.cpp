//
#include "DecalFinder.h"


CModel::CModel(string sID)
{
    m_ID = sID;
    m_bLearningDone = false;
    m_f1stScore = 0;
    m_bIsActivated = true;

    m_nRatio = 4;
    m_fAcceptScore = 0.0f;

    m_fMaxRotation = 0.0f;
    m_fMinRotation = 0.0f;
    m_fRotationIncr = 1.0f;

    m_fMaxScale = 1;
    m_fMinScale = 1;
    m_fScaleIncr = 0.1F;

    //Pubic results
    m_fScore = 0;
    m_fScale = 1;
    m_fAngle = 0;

    m_nRotSample = 0;
    m_nScaleSample = 0;
}

CModel::~CModel()
{

}
void CModel::ActivateRotation(int rotIdx)
{
    if (m_ActivatedRot.size() > rotIdx)
        m_ActivatedRot[rotIdx] = true;
}

void CModel::DeActivateRotation(int rotIdx)
{
    if (m_ActivatedRot.size() > rotIdx)
        m_ActivatedRot[rotIdx] = false;
}

int CModel::GetRotationIndex(int rotAngle)
{
    int rotIdx = -1;
    for (size_t i = 0; i < m_nRotSample - 1; i++)
    {
        if (m_Rotation[i] <= rotAngle&&m_Rotation[i + 1]>rotAngle)
        {
            rotIdx = i;
            break;
        }
    }
    return rotIdx;
}
/*
void CModel::ActivateScale(int scaleIdx)
{
    if (m_ActivatedScale.size() > scaleIdx)
        m_ActivatedScale[scaleIdx] = true;
}
*/
/*
void CModel::DeActivateScale(int scaleIdx)
{
    if (m_ActivatedScale.size() > scaleIdx)
        m_ActivatedScale[scaleIdx] = false;
}
*/
CDecalFinder::CDecalFinder()
{
    m_nNumOfModel = 0;
	m_nBestMatchIdx = 0;
}

CDecalFinder::~CDecalFinder()
{
}

/**
 * @Brief AddModel
 * @Return 0/-1
 */
int CDecalFinder::AddModel(cv::Mat modelData, cv::Size szModel, float minRot, float maxRot, float rotIncr,
    float minScale, float maxScale, float scaleIncr, float acceptScore, string sID)
{
    try
    {
        CModel temp(sID);

        if (szModel.width < 40 || szModel.height < 40) temp.m_nRatio = 2;
        if (szModel.width < 10 || szModel.height < 10) temp.m_nRatio = 1;

        temp.m_nRotSample = 1;

        if (minRot < -90) minRot = -90;  //only support rotation from -90 to 90
        if (maxRot > 90) maxRot = 90;
        if (maxRot > minRot && rotIncr > FLT_EPSILON)
        {
            temp.m_nRotSample = (int)(1 + (maxRot - minRot) / rotIncr);

            for (int i = 0; i < temp.m_nRotSample; i++) {
                temp.m_Rotation.push_back(minRot + i * rotIncr);
                temp.m_ActivatedRot.push_back(true);
            }
        }
        else
        {
            temp.m_Rotation.push_back(minRot);
        }

        temp.m_nScaleSample = 1;
        if (maxScale > minScale && scaleIncr > FLT_EPSILON)
        {
            temp.m_nScaleSample = (int)(1 + (maxScale - minScale) / scaleIncr);

            //m_Scale = new float[m_nScaleSample];
            for (int i = 0; i < temp.m_nScaleSample; i++) {
                temp.m_Scale.push_back(minScale + i * scaleIncr);
                temp.m_ActivatedScale.push_back(true);
            }
        }
        else {
            temp.m_Scale.push_back(minScale);
        }

        temp.m_ModelData = modelData;
        temp.m_sModelSize = szModel;
        temp.m_fMaxRotation = maxRot;
        temp.m_fMinRotation = minRot;
        temp.m_fRotationIncr = rotIncr;
        temp.m_fMaxScale = maxScale;
        temp.m_fMinScale = minScale;
        temp.m_fScaleIncr = scaleIncr;
        temp.m_fAcceptScore = acceptScore;

        int ret = temp.fnCreateSample();
        if (ret != 0) {
            return ret;
        }

        temp.m_bLearningDone = true;

        m_nNumOfModel++;
        m_listModel.push_back(temp);
    }
    catch (...)
    {
        CLogger::log(LOG_EXCEPTION, " CDecalFinder::AddModel(...) throw unknown exception error");
        return -1;
    }

    return 0;
}

/**
 * @Brief fnCreateSample
 * @Return 0/-1
 */
int CModel::fnCreateSample()
{
    if (m_ModelData.empty()) {
        CLogger::log(LOG_ERROR, "CModel::fnCreateSample() - model data is empty");
        return -1;
    }

    //int nSrcStep = 4 * (1 + (m_sModelSize.width - 1) / 4);
    int nBackground = (*m_ModelData.ptr(0, 0) + *m_ModelData.ptr(0, m_sModelSize.width - 1) +
        *m_ModelData.ptr(m_sModelSize.height - 1, m_sModelSize.width - 1) + *m_ModelData.ptr(m_sModelSize.height - 1, 0)) / 4;

    for (int i = 0; i < m_nRotSample; i++)
    {
        if (m_sModelSize.width < m_sModelSize.height)
        {
            float nAngle = abs(m_Rotation[i]);
            Size szOutput;
            Mat rotation;
            if (nAngle <= 45)
            {
                double sa = sin(nAngle * RADIAN_PER_DEGREE);
                double ca = cos(nAngle * RADIAN_PER_DEGREE);

                if (std::abs(ca) <= DBL_EPSILON) {
                    //divide by zero
                    CLogger::log(LOG_ERROR, "CModel::fnCreateSample() - std::abs(ca) <= DBL_EPSILON");
                    return -1;
                }

                int nNewHeight = (int)(((double)m_sModelSize.height - (double)m_sModelSize.width * sa) / ca);
                nNewHeight = nNewHeight - ((m_sModelSize.height - nNewHeight) % 2);
                szOutput = Size(m_sModelSize.width, nNewHeight);
            }
            else
            {
                double sa = sin((90 - nAngle) * RADIAN_PER_DEGREE);
                double ca = cos((90 - nAngle) * RADIAN_PER_DEGREE);
                if (std::abs(ca) <= DBL_EPSILON) {
                    //divide by zero
                    CLogger::log(LOG_ERROR, "CModel::fnCreateSample() - std::abs(ca) <= DBL_EPSILON");
                    return -1;
                }

                int nNewWidth = (int)(((double)m_sModelSize.height - (double)m_sModelSize.width * sa) / ca);
                nNewWidth = nNewWidth - ((m_sModelSize.width - nNewWidth) % 2);
                szOutput = Size(nNewWidth, m_sModelSize.width);

            }

            CUtil::fnRotateCenter(m_ModelData, m_sModelSize, rotation, szOutput, m_Rotation[i], nBackground);

            for (int j = 0; j < m_nScaleSample; j++)
            {
                int nX = (int)((float)szOutput.width * m_Scale[j]);
                int nY = (int)((float)szOutput.height * m_Scale[j]);
                //int nIndex
                Samples temp;
				temp.rotation = 0;
				temp.scale = 1.0;
                temp.size = Size(nX, nY);
                CUtil::fnResize(rotation, szOutput, m_Scale[j], temp.size, temp.data);
                m_SamplesList.push_back(temp);
            }
        }
        else
        {
            float nAngle = abs(m_Rotation[i]);
            Size szOutput;
            Mat rotation;
            if (nAngle <= 45)
            {
                double sa = sin(nAngle * RADIAN_PER_DEGREE);
                double ca = cos(nAngle * RADIAN_PER_DEGREE);
                if (std::abs(ca) <= DBL_EPSILON) {
                    //divide by zero
                    CLogger::log(LOG_ERROR, "CModel::fnCreateSample() - std::abs(ca) <= DBL_EPSILON");
                    return -1;
                }

                int nNewWidth = (int)(((double)m_sModelSize.width - (double)m_sModelSize.height * sa) / ca);
                nNewWidth = nNewWidth - ((m_sModelSize.width - nNewWidth) % 2);
                szOutput = Size(nNewWidth, m_sModelSize.height);
            }
            else
            {
                double sa = sin((90 - nAngle) * RADIAN_PER_DEGREE);
                double ca = cos((90 - nAngle) * RADIAN_PER_DEGREE);
                if (std::abs(ca) <= DBL_EPSILON) {
                    //divide by zero
                    CLogger::log(LOG_ERROR, "CModel::fnCreateSample() - std::abs(ca) <= DBL_EPSILON");
                    return -1;
                }

                int nNewHeight = (int)(((double)m_sModelSize.width - (double)m_sModelSize.height * sa) / ca);
                nNewHeight = nNewHeight - ((m_sModelSize.height - nNewHeight) % 2);
                szOutput = Size(m_sModelSize.height, nNewHeight);

            }

            CUtil::fnRotateCenter(m_ModelData, m_sModelSize, rotation, szOutput, m_Rotation[i], nBackground);

            for (int j = 0; j < m_nScaleSample; j++)
            {
                int nX = (int)((float)szOutput.width * m_Scale[j]);
                int nY = (int)((float)szOutput.height * m_Scale[j]);
                Samples temp;
				temp.rotation = 0;
				temp.scale = 1.0;
                temp.size = Size(nX, nY);
                CUtil::fnResize(rotation, szOutput, m_Scale[j], temp.size, temp.data);
                m_SamplesList.push_back(temp);
            }
        }
    }

    return 0;
}

/**
 * @Brief Execute finding decal image type based on template macthing algorithm
 * @Return 0/-1
 */
int CDecalFinder::Execute(cv::Mat src, cv::Size szSrc)
{
    try
    {
        float fBestFinalScore = 0;
        m_nBestMatchIdx = -1;
        for (int n = 0; n < m_nNumOfModel; n++)
        {
            m_listModel[n].m_f1stScore = 0;
            m_listModel[n].m_fScore = 0;
            if (!m_listModel[n].m_bIsActivated) {
                continue;
            }

            if (m_listModel[n].m_nRatio == 0) {
                CLogger::log(LOG_WARNING, "CDecalFinder::Execute(...) m_listModel[n].m_nRatio == 0 -> div by zero");
                continue;
            }

            double dSCALE = (double)1 / (double)m_listModel[n].m_nRatio;

            //Resize source by RATIO
            int nXsrc = (int)((double)szSrc.width * dSCALE);
            int nYsrc = (int)((double)szSrc.height * dSCALE);

            Mat srcRes;
            Size szSrcRes = Size(nXsrc, nYsrc);
            CUtil::fnResize(src, szSrc, dSCALE, szSrcRes, srcRes);

            for (int i = 0; i < m_listModel[n].m_nRotSample; i++)
            {
                if (!m_listModel[n].m_ActivatedRot[i])
                    continue;
                for (int j = 0; j < m_listModel[n].m_nScaleSample; j++)
                {
                    if (!m_listModel[n].m_ActivatedScale[j])
                        continue;
                    //Resize template by RATIO
                    Mat tmpRes;
                    int nIndex = i*m_listModel[n].m_nScaleSample + j;
                    int nXtmp = (int)((double)m_listModel[n].m_SamplesList[nIndex].size.width * dSCALE);
                    int nYtmp = (int)((double)m_listModel[n].m_SamplesList[nIndex].size.height * dSCALE);

                    Size szTmpRes = Size(nXtmp, nYtmp);
                    CUtil::fnResize(m_listModel[n].m_SamplesList[nIndex].data, m_listModel[n].m_SamplesList[nIndex].size, dSCALE, szTmpRes, tmpRes);

                    //Calculate NCC of first phase
                    Point2f firstPos;
                    float fFirstScore = 100 * fnTM_1stPhase_Gray(srcRes, tmpRes, firstPos);
                    if (/*fFirstScore < 0.95 * m_listModel[n].m_fAcceptScore ||*/ fFirstScore < 0.95 * m_listModel[n].m_f1stScore) {
                        continue;
                    }

                    float fFinalScore = fFirstScore;
                    Point2f finalPos = firstPos;
                    m_listModel[n].m_f1stScore = fFirstScore;

                    //Calculate NCC of second phase
                    if (dSCALE == 1)
                    {
                        if (fFinalScore < m_listModel[n].m_fAcceptScore || fFinalScore < m_listModel[n].m_fScore) {
                            continue;
                        }
                    }
                    else
                    {
                        fFinalScore = 100 * fnTM_2ndPhase_Gray(src, n, m_listModel[n].m_SamplesList[nIndex].data, firstPos, finalPos);
                        if (/*fFinalScore < m_listModel[n].m_fAcceptScore ||*/ fFinalScore < m_listModel[n].m_fScore) {
                            continue;
                        }
                    }

                    //Save result
                    m_listModel[n].m_fScore = fFinalScore;
                    m_listModel[n].m_matchPosition = Point2f(finalPos.x + (float)m_listModel[n].m_SamplesList[nIndex].size.width /
                        2.0f, finalPos.y + (float)m_listModel[n].m_SamplesList[nIndex].size.height / 2.0f);  //center

                    m_listModel[n].m_fAngle = m_listModel[n].m_Rotation[i];
                    m_listModel[n].m_fScale = m_listModel[n].m_Scale[j];
                }
            }

            if (m_listModel[n].m_fScore > fBestFinalScore&&m_listModel[n].m_fScore > m_listModel[n].m_fAcceptScore) {
                fBestFinalScore = m_listModel[n].m_fScore;
                m_nBestMatchIdx = n;

            }
        }

        if (m_nBestMatchIdx > -1)
        {
            //Get rectangle
            float x0 = (float)m_listModel[m_nBestMatchIdx].m_matchPosition.x;
            float y0 = (float)m_listModel[m_nBestMatchIdx].m_matchPosition.y;

            float x1 = x0 - ((float)m_listModel[m_nBestMatchIdx].m_sModelSize.width / 2.0f) * m_listModel[m_nBestMatchIdx].m_fScale;
            float y1 = y0 - ((float)m_listModel[m_nBestMatchIdx].m_sModelSize.height / 2.0f) * m_listModel[m_nBestMatchIdx].m_fScale;
            float x2 = x0 + ((float)m_listModel[m_nBestMatchIdx].m_sModelSize.width / 2.0f) * m_listModel[m_nBestMatchIdx].m_fScale;
            float y2 = y0 + ((float)m_listModel[m_nBestMatchIdx].m_sModelSize.height / 2.0f) * m_listModel[m_nBestMatchIdx].m_fScale;

            // Coordinates of the rotated rectangle
            double ca = cos(m_listModel[m_nBestMatchIdx].m_fAngle * RADIAN_PER_DEGREE);
            double sa = sin(m_listModel[m_nBestMatchIdx].m_fAngle * RADIAN_PER_DEGREE);

            double rx1 = (x0 + (x1 - x0) * ca - (y1 - y0) * sa);
            double ry1 = (y0 + (x1 - x0) * sa + (y1 - y0) * ca);
            double rx2 = (x0 + (x2 - x0) * ca - (y1 - y0) * sa);
            double ry2 = (y0 + (x2 - x0) * sa + (y1 - y0) * ca);
            double rx3 = (x0 + (x2 - x0) * ca - (y2 - y0) * sa);
            double ry3 = (y0 + (x2 - x0) * sa + (y2 - y0) * ca);
            double rx4 = (x0 + (x1 - x0) * ca - (y2 - y0) * sa);
            double ry4 = (y0 + (x1 - x0) * sa + (y2 - y0) * ca);

            m_listModel[m_nBestMatchIdx].m_recResult[0] = Point2f((float)rx1, (float)ry1);
            m_listModel[m_nBestMatchIdx].m_recResult[1] = Point2f((float)rx2, (float)ry2);
            m_listModel[m_nBestMatchIdx].m_recResult[2] = Point2f((float)rx3, (float)ry3);
            m_listModel[m_nBestMatchIdx].m_recResult[3] = Point2f((float)rx4, (float)ry4);
        }
    }
    catch (...)
    {
        CLogger::log(LOG_EXCEPTION, "CDecalFinder::Execute(...) throw unknown exception error");
        return -1;
    }

    return 0;
}

/**
 * @Brief fnTM_1stPhase_Gray
 * @Return score value
 */
float CDecalFinder::fnTM_1stPhase_Gray(const Mat& srcRes, const Mat& tmpRes, Point2f &firstPos)
{
    if (srcRes.cols - tmpRes.cols + 1 < 1 || srcRes.rows - tmpRes.rows + 1 < 1) {
        return 0;
    }

    double minVal;
    double maxVal;

    try
    {
        Mat result = Mat(srcRes.cols - tmpRes.cols + 1, srcRes.rows - tmpRes.rows + 1, CV_32FC1);
        matchTemplate(srcRes, tmpRes, result, 5);

        Point minLoc;
        Point maxLoc;
        Point matchLoc;

        minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
        matchLoc = maxLoc; //for method=3

        firstPos = Point2f((float)matchLoc.x, (float)matchLoc.y);
    }
    catch (...)
    {
        CLogger::log(LOG_EXCEPTION, "CDecalFinder::fnTM_1stPhase_Gray(...) throw unknown exception error");
        return -1.0f;
    }

    return (float)maxVal;
}

/**
 * @Brief fnTM_2ndPhase_Gray
 * @Return score value
 */
float CDecalFinder::fnTM_2ndPhase_Gray(const cv::Mat& src, int modelIdx, const cv::Mat& tmp, const cv::Point2f& firstPos, cv::Point2f &finalPos)
{
    float fFinalScore = 0;
    double minVal;
    double maxVal;

    try
    {
        Point leftTop = Point(0, 0);
        Point rightBot = Point(0, 0);


        leftTop.x = (int)max(m_listModel[modelIdx].m_nRatio * firstPos.x - m_listModel[modelIdx].m_nRatio / 2, 0);
        leftTop.y = (int)max(m_listModel[modelIdx].m_nRatio * firstPos.y - m_listModel[modelIdx].m_nRatio / 2, 0);

        rightBot.x = (int)min(m_listModel[modelIdx].m_nRatio * firstPos.x + m_listModel[modelIdx].m_nRatio / 2 + tmp.cols, src.cols - 1);
        rightBot.y = (int)min(m_listModel[modelIdx].m_nRatio * firstPos.y + m_listModel[modelIdx].m_nRatio / 2 + tmp.rows, src.rows - 1);

        Size szSrcCrop = Size(rightBot.x - leftTop.x + 1, rightBot.y - leftTop.y + 1);

        Mat srcCrop;
        cv::Rect roi = cv::Rect(leftTop.x, leftTop.y, szSrcCrop.width, szSrcCrop.height);
        srcCrop = src(roi);
        //IPP.fnCrop(src, szSrc, new Rectangle(leftTop, szSrcCrop), out srcCrop);

        Size szDstCrop = Size(rightBot.x - leftTop.x - tmp.cols + 1, rightBot.y - leftTop.y - tmp.rows + 1);
        if (szDstCrop.width < 1 || szDstCrop.height < 1)
        {
            finalPos = Point2f(0, 0);
            return 0;
        }

        Mat result = Mat(srcCrop.cols - tmp.cols + 1, srcCrop.rows - tmp.rows + 1, CV_32FC1);
        matchTemplate(srcCrop, tmp, result, 5);

        Point minLoc;
        Point maxLoc;
        Point matchLoc;

        minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
        matchLoc = maxLoc; //for method=3
        fFinalScore = (float)maxVal;

        finalPos = Point2f((float)(matchLoc.x + leftTop.x), (float)(matchLoc.y + leftTop.y));
    }
    catch (...)
    {
        CLogger::log(LOG_EXCEPTION, "CDecalFinder::fnTM_2ndPhase_Gray(...) throw unknown exception error");
        return -1.0f;
    }

    return fFinalScore;
}
/*
int CDecalFinder::GetModelIndexByID(string modelID)
{
    for (size_t i = 0; i < m_listModel.size(); i++)
    {
        if (m_listModel[i].m_ID == modelID)
            return i;
    }
    return -1;
}
*/

void CDecalFinder::DeactivateModel(int modelIdx)
{
    m_listModel[modelIdx].m_bIsActivated = false;
}

void CDecalFinder::ActivateModel(int modelIdx)
{
    m_listModel[modelIdx].m_bIsActivated = true;
}


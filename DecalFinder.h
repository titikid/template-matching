#pragma once

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"

#include <iostream>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <windows.h>

#include "Util.h"
#include "Logger.h"

using namespace std;
using namespace cv;

#define RADIAN_PER_DEGREE  0.0174532F

struct Samples
{
    cv::Mat data;
    cv::Size size;
    int rotation;
    int scale;
};

class CModel
{
public:
    CModel(string sID);
    ~CModel();

    int fnCreateSample();
    void ActivateRotation(int rotIdx);
    void DeActivateRotation(int rotIdx);
    int GetRotationIndex(int rotAngle);

    //void ActivateScale(int scaleIdx);
    //void DeActivateScale(int scaleIdx);

public:

    string m_ID;
    int m_nRatio;
    float m_fAcceptScore;

    float m_fMaxRotation;
    float m_fMinRotation;
    float m_fRotationIncr;

    float m_fMaxScale;
    float m_fMinScale;
    float m_fScaleIncr;			//Scale increment

    //Pubic results
    float m_fScore;				//result's score
    float m_fScale;				//result's Scale
    float m_fAngle;				//result's angle
    Point m_matchPosition;		//Center
    Point m_recResult[4];		//Coordinate of results
    bool m_bLearningDone;
    bool m_bIsActivated;

    cv::Mat m_ModelData;		//origin
    cv::Size m_sModelSize;

    std::vector<float> m_Rotation;
    std::vector<bool> m_ActivatedRot;
    std::vector<float> m_Scale;
    std::vector<bool> m_ActivatedScale;
    int m_nRotSample;
    int m_nScaleSample;
    float m_f1stScore;

    std::vector<Samples> m_SamplesList;
};

class CDecalFinder
{
public:
    CDecalFinder();
    ~CDecalFinder();

public:
    /**
     * @Brief AddModel
     */
    int AddModel(cv::Mat modelData, cv::Size szModel, float minRot, float maxRot, float rotIncr,
        float minScale, float maxScale, float scaleIncr, float acceptScore, string sID);

    /**
     * @Brief Execute
     */
    int Execute(cv::Mat src, cv::Size szSrc);

    /**
     * @Brief GetResult
     */
    int GetResult(int modelIdx);

    //int GetModelIndexByID(string modelID);

    /**
    * @Brief DeactivateModel
    */
    void DeactivateModel(int modelIdx);

    /**
    * @Brief ActivateModel
    */
    void ActivateModel(int modelIdx);

private:

    float fnTM_1stPhase_Gray(const cv::Mat& srcRes, const cv::Mat& tmpRes, cv::Point2f &firstPos);
    float fnTM_2ndPhase_Gray(const cv::Mat& src, int modelIdx, const cv::Mat& tmp, const cv::Point2f& firstPos, cv::Point2f &finalPos);

public:
    std::vector<CModel> m_listModel;
    string m_id;
    int m_nNumOfModel;
    int m_nBestMatchIdx;
};

